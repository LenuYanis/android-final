package com.example.proiectdam19;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.itextpdf.text.Document;
import com.itextpdf.text.Header;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class HomePage extends AppCompatActivity {
    private final static String TAG= HomePage.class.getName();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);
        isReadStoragePermissionGranted();
        isWriteStoragePermissionGranted();

    }

    public void EnterElem(View View){
        Intent intent= new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void savePdf(View view) {
        Document document = new Document(PageSize.A4);
        String mFileName = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(System.currentTimeMillis());
        String mFilePath = Environment.getExternalStorageDirectory() + "/" + mFileName + ".pdf";
        RoomDataBase dbInstance = RoomDataBase.getINSTANCE(getApplicationContext());

        try {
            PdfWriter.getInstance(document, new FileOutputStream(mFilePath));
            document.open();

//            get the dao instance from db instance
            DAOitems daoItem = dbInstance.getDAOitems();
            //the item service receive the daoItem in order to do it
            final Service itemService = new Service(daoItem);

            Cursor i = null;
            i = itemService.getItems();
            while (i.moveToNext()) {
                int NameColumn = i.getColumnIndex("Name");
                int priceColumn = i.getColumnIndex("TotalPrice");
                int quantityColumn = i.getColumnIndex("Quantity");
                String name = i.getString(NameColumn);
                String price = i.getString(priceColumn);
                String Quantity = i.getString(quantityColumn);
                document.add(new Header("Report", "Report"));
                document.add(new Paragraph("Object Type: " + name + "  //  Quantity: " + Quantity + "  //  TotalPrice: " + price));
            }


            document.close();
            Toast.makeText(getApplicationContext(), "Exported!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();

        }


    }

    public void btnDD(View view) {
        Intent intent = new Intent(this, ReadDataBaseActivity.class);
        startActivity(intent);
    }

    public void btnJSON(View view) {
        Intent intent = new Intent(this, JSONactivity.class);
        startActivity(intent);

    }
    public void buttonPie(View view){
        Intent intent= new Intent(this, PieChartActivity.class);
        startActivity(intent);
    }

    public  boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted1");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked1");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted1");
            return true;
        }
    }

    public  boolean isWriteStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted2");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked2");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted2");
            return true;
        }
    }

}
