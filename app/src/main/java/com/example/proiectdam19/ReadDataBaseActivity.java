package com.example.proiectdam19;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class ReadDataBaseActivity extends AppCompatActivity {
    private static final String TAG=ReadDataBaseActivity.class.getName();

    private static List<Items> itemList= new ArrayList<>();

    private ListView itemListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dblist);


        RoomDataBase dbInstance = RoomDataBase.getINSTANCE(getApplicationContext());
        //get the dao instance from db instance
        DAOitems daoItem = dbInstance.getDAOitems();
        //the item service receive the daoItem in order to do it
        final Service itemService = new Service(daoItem);

        Cursor i = null;
        i = itemService.getItems();
        try {
            while (i.moveToNext()) {
                int NameColumn = i.getColumnIndex("Name");
                int priceColumn = i.getColumnIndex("TotalPrice");
                int quantityColumn = i.getColumnIndex("Quantity");
                String name = i.getString(NameColumn);
                String price = i.getString(priceColumn);
                String Quantity = i.getString(quantityColumn);

                Items it=new Items();
                it.setName(name.toString());
                it.setQuantity(Integer.parseInt(Quantity));
                it.setTotalPrice(Integer.parseInt(price));


                if (it != null) {
                    itemList.add(it);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        itemListView = findViewById(R.id.dbLIST);

        final ArrayAdapter<Items> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, itemList);
        itemListView.setAdapter(arrayAdapter);

        final ReadDataBaseAdapter iadapter = new ReadDataBaseAdapter(this, itemList);
        itemListView.setAdapter(iadapter);


        itemListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


                itemList.remove(position);
                arrayAdapter.notifyDataSetChanged();
                itemListView.setAdapter(iadapter);
                Toast.makeText(getApplicationContext(), "Item Deleted!", Toast.LENGTH_SHORT).show();
                return  true;
            }
        });
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

    }


}
