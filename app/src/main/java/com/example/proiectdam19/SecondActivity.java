package com.example.proiectdam19;



import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class SecondActivity extends AppCompatActivity {
    private static final String TAG=SecondActivity.class.getName();

    private static List<Items> itemList= new ArrayList<>();

    private ListView itemListView;

    private Button btnDB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        btnDB=findViewById(R.id.button4);

        Intent intent=getIntent();
        Items item=intent.getParcelableExtra("itemKey");
        if(item!=null){ itemList.add(item);  }
        itemListView=findViewById(R.id.lvItems);

        ArrayAdapter<Items> arrayAdapter=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,itemList);
        itemListView.setAdapter(arrayAdapter);

        ItemsAdapter iadapter= new ItemsAdapter(this,itemList);
        itemListView.setAdapter(iadapter);



    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

    }

    public void btnDB(View view){
        RoomDataBase dbInstance= RoomDataBase.getINSTANCE(getApplicationContext());
        DAOitems dao=dbInstance.getDAOitems();
        final Service svc= new Service(dao);
        for(Items i: itemList){
                svc.addItem(i);
        }

        Toast.makeText(getApplicationContext(), "Added" , Toast.LENGTH_SHORT).show();
    }
}
