package com.example.proiectdam19;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import org.w3c.dom.Text;

import java.util.List;
import java.util.Objects;

public class ItemsAdapter extends BaseAdapter {

    private Context ctx;
    private List<Items> obje;


    public ItemsAdapter(Context context, List<Items> ilist) {
        this.ctx = context;
        this.obje = ilist;
    }

    @Override
    public int getCount() {
        return obje.size();
    }

    @Override
    public Object getItem(int position) {
        return obje.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemHolder itemHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(R.layout.android_items, parent, false);
            itemHolder = new ItemHolder(convertView);

        } else {
            itemHolder = (ItemHolder) convertView.getTag();

        }

        Items it = (Items) getItem(position);
        itemHolder.tvTitle.setText( "Name: "+ it.getName());
        itemHolder.tvTitle2.setText("TotalPrice: "+ String.valueOf(it.getTotalPrice()));
        itemHolder.tvTitle3.setText("Quantity: "+ String.valueOf(it.getQuantity()));

        return convertView;

    }

    private class ItemHolder {
        public TextView tvTitle;
        public TextView tvTitle2;
        public TextView tvTitle3;
        ItemHolder(View view) {
            tvTitle = view.findViewById(R.id.tvName2);
            tvTitle2=view.findViewById(R.id.tvName3);
            tvTitle3=view.findViewById(R.id.tvName4);
        }
    }
}


