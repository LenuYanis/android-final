package com.example.proiectdam19;

import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class PieChartActivity extends AppCompatActivity {
    private PieChart pie;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pie_chart);
        pie = findViewById(R.id.pieChart);
        pie.setUsePercentValues(false);
        pie.setHoleRadius(0);
        pie.animateXY(500,500);
        pie.setTransparentCircleRadius(0);

        List<PieEntry> value= new ArrayList<>();

        RoomDataBase dbInstance = RoomDataBase.getINSTANCE(getApplicationContext());

        try {
//           get the dao instance from db instance
            DAOitems daoItem = dbInstance.getDAOitems();
            //the item service receive the daoItem in order to do it
            final Service itemService = new Service(daoItem);

            Cursor i = null;
            i = itemService.getItems();
            while (i.moveToNext()) {
                int NameColumn = i.getColumnIndex("Name");
                int quantityColumn = i.getColumnIndex("Quantity");
                String Quantity = i.getString(quantityColumn);
                String name = i.getString(NameColumn);
                value.add(new PieEntry(Integer.parseInt(Quantity),name));

            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        PieDataSet piedataset=new PieDataSet(value,"Objects");
        PieData pieData= new PieData(piedataset);
        piedataset.setColors(ColorTemplate.MATERIAL_COLORS);
        piedataset.setValueTextSize(25);

        pie.setData(pieData);





    }
}
