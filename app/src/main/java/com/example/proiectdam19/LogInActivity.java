package com.example.proiectdam19;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;

public class LogInActivity extends AppCompatActivity {

    private EditText UserName;
    private EditText PassWord;
    private Button Login;
    private Button Location;
    private ImageView img;
    private static final int PICK_IMAGE = 100;
    Uri imgUri;
    MapActivity mMap;


    public static final String Mypref = "Myprefs";
    public static final String UserNM = "nameKey";
    public static final String Passw = "passKey";

    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        UserName = findViewById(R.id.UserNameET);
        PassWord = findViewById(R.id.PasswordET);
        Login = findViewById(R.id.btnLogIN);
        Location = findViewById(R.id.btnLocation);
        img = (ImageView) findViewById(R.id.imageView);
        int imageresource = getResources().getIdentifier("@drawable/profile", null, this.getPackageName());
        img.setImageResource(imageresource);

        sp = getSharedPreferences(Mypref, Context.MODE_PRIVATE);



    }

    public void btnLogIN(View view) {

        String u = UserName.getText().toString();
        String p = PassWord.getText().toString();
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(UserNM, u);
        editor.putString(Passw, p);
        editor.commit();

        if (UserName.getText().toString().equalsIgnoreCase("Admin") && PassWord.getText().toString().equalsIgnoreCase("admin")) {

            Intent intent = new Intent(this, HomePage.class);
            startActivity(intent);

        } else {
            Toast.makeText(getApplicationContext(), "Wrong user or pass", Toast.LENGTH_SHORT).show();
        }


    }

    public void btnLOcation(View view) {


        Intent intent = new Intent(this, MapActivity.class);

        startActivity(intent);

    }


    public void ImageChange(View view) {
        openGallery();
    }

    private void openGallery() {
        Intent galerry = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(galerry, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imgUri = data.getData();
            img.setImageURI(imgUri);
        }
    }

}

