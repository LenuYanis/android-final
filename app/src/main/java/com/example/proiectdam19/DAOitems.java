package com.example.proiectdam19;


import android.database.Cursor;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;


@Dao
public interface DAOitems {

    @Query("SELECT * FROM Items")
    Cursor getItems();



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long addItem(Items item);

    @Delete
    void deleteItem(Items item);


}
