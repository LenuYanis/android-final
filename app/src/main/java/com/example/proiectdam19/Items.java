package com.example.proiectdam19;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;
@Entity(tableName = "Items")
public class Items implements Parcelable {
    @PrimaryKey
    @NonNull
    private String Name;
    @NonNull
    private Integer Quantity;
    @NonNull
    private Integer TotalPrice;

    @TypeConverters(DateConverter.class)
    private Date Date;

    public Items(String iName, Integer iQuantity, Integer iTotalPrice, Date iDate) {
        this.Name = iName;
        this.Quantity = iQuantity;
        this.TotalPrice = iTotalPrice;
        this.Date = iDate;
    }

    public Items(){}


    @NonNull
    public String getName() {
        return Name;
    }

    public void setName(String iName) {
        this.Name = iName;
    }

    public Integer getQuantity() {
        return Quantity;
    }

    public void setQuantity(int iQuantity) {
        this.Quantity = iQuantity;
    }

    public Integer getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(int iTotalPrice) {
        this.TotalPrice = iTotalPrice;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date iDate) {
        this.Date = iDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    protected Items(Parcel in){
        Name=in.readString();
        if(in.readByte()==0){
            Quantity=null;
        }
        else{
            Quantity=in.readInt();
        }
        if(in.readByte()==0){
            TotalPrice=null;
        }
        else{
            TotalPrice=in.readInt();
        }

        if(in.readByte()==0){
            Date=null;
        }
        else{
            Date=(Date)in.readSerializable();
        }



    }
    //SERIALIZARE!!
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Name);

        if (Quantity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Quantity);
        }

        if(TotalPrice ==null)
            dest.writeByte((byte)0);
            else{
                dest.writeByte((byte)1);
                dest.writeInt(TotalPrice);
        }

            if(Date==null){
                dest.writeByte((byte)0);

            }
            else{
                dest.writeByte((byte)0);
                dest.writeSerializable(Date);
            }

    }


    public static final Creator<Items> CREATOR = new Creator<Items>(){

        @Override
        public Items createFromParcel(Parcel in) {
            return new Items(in);
        }

        @Override
        public Items[] newArray(int size) {
            return new Items[size];
        }
    };

    @Override
    public String toString() {
        return "Items{" +
                "iName='" + Name + '\'' +
                ", iQuantity=" + Quantity +
                ", iTotalPrice=" + TotalPrice +
                ", iDate=" + Date +
                '}';
    }
}
