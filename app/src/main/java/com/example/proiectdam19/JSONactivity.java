package com.example.proiectdam19;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class JSONactivity extends ListActivity {
    private ProgressDialog pDialog;

    private static final String TAG_ITEMS="Items";

    private static final String TAG_NAME="Name";
    private static final String TAG_QUANTITY="Quantity";
    private static final String TAG_PRICE="Price";
    private static final String TAG_ORDERDATE="OrderDate";

    JSONArray items=null;

    ArrayList<HashMap<String,String>>itemList;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);

        itemList = new ArrayList<HashMap<String, String>>();
        URL url = null;
        try {
            url = new URL("https://api.myjson.com/bins/id74c");

        }
        catch (MalformedURLException ex) {
            ex.printStackTrace();
        }

        GetItems i=new GetItems();
        i.setEvent(new OnTaskExecutionFinished(){
            public void OnTaskFinishedEvent(String result){
//                if(pDialog.isShowing()){
//                    try{
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    pDialog.dismiss();
//                }

                ListAdapter adapter= new SimpleAdapter(
                        JSONactivity.this,
                        itemList,
                        R.layout.item_list,
                        new String[]{TAG_NAME,TAG_QUANTITY,TAG_PRICE,TAG_ORDERDATE},
                        new int[]{R.id.Name,R.id.Quantity,R.id.Price,R.id.OrderDate}
                )
                {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        return super.getView(position, convertView, parent);
                    }
                };
                setListAdapter(adapter);
            }
        });
        i.execute(url);
    }


    public interface OnTaskExecutionFinished {
        void OnTaskFinishedEvent(String result);
    }


    public class GetItems extends AsyncTask<URL, Void, String> {
        private OnTaskExecutionFinished event;

        public void setEvent(OnTaskExecutionFinished event) {
            if (event != null)
                this.event = event;
        }

        @Override
        protected String doInBackground(URL... urls) {
            HttpURLConnection conn = null;

            try {
                conn = (HttpURLConnection) urls[0].openConnection();
                conn.setRequestMethod("GET");
                InputStream ist = conn.getInputStream();

                InputStreamReader isr = new InputStreamReader(ist);
                BufferedReader reader = new BufferedReader(isr);
                String line = "";
                String buffer = "";
                while ((line = reader.readLine()) != null) {
                    buffer += line;
                }
                LoadJSONobject(buffer);
                return buffer;

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (conn != null)
                    conn.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (this.event != null) {
                this.event.OnTaskFinishedEvent(s);
            } else {
                Log.d("GetItems", "Event is null");
            }
        }


        public void LoadJSONobject(String jsonStr) {
            if (jsonStr != null) {
                try {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(jsonStr);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    items = jsonObject.getJSONArray(TAG_ITEMS);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject c = items.getJSONObject(i);

                        String Name = c.getString(TAG_NAME);
                        String Quantity = c.getString(TAG_QUANTITY);
                        String Price = c.getString(TAG_PRICE);
                        String OrderDate = c.getString(TAG_ORDERDATE);

                        HashMap<String, String> item = new HashMap<String, String>();
                        item.put(TAG_NAME, Name);
                        item.put(TAG_QUANTITY, Quantity);
                        item.put(TAG_PRICE, Price);
                        item.put(TAG_ORDERDATE, OrderDate);
                        itemList.add(item);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else
            {
                Log.e("loadJsonObject", "Couldn't get any data from json");
            }
        }
    }


}



