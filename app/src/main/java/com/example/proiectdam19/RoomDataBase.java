package com.example.proiectdam19;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
@Database( entities ={Items.class}, version = 1,exportSchema = false)
public abstract class RoomDataBase extends RoomDatabase {

    private static RoomDataBase INSTANCE;

    public static RoomDataBase getINSTANCE(Context context){

        if(INSTANCE==null){
                synchronized (RoomDataBase.class){
                        if(INSTANCE==null){
                            INSTANCE= Room.databaseBuilder(context.getApplicationContext(),RoomDataBase.class,"Items.db")
                                    .allowMainThreadQueries().build();
                        }
                }
        }
        return INSTANCE;
    }

    public abstract DAOitems getDAOitems();

}
