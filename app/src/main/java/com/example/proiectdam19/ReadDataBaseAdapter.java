package com.example.proiectdam19;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ReadDataBaseAdapter extends BaseAdapter {
    private Context ctx;
    private List<Items> list;

    public ReadDataBaseAdapter (Context context, List<Items> ilist) {
        this.ctx = context;
        this.list = ilist;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DBholder db=null;


        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(R.layout.dbtextview, parent, false);
            db = new DBholder(convertView);

        } else {
            db = (DBholder) convertView.getTag();

        }
        Items it = (Items) getItem(position);
        db.dbTitle.setText( "Name: "+ it.getName());
        db.dbTitle2.setText("TotalPrice: "+ String.valueOf(it.getTotalPrice()));
        db.dbTitle3.setText("Quantity: "+ String.valueOf(it.getQuantity()));

        return convertView;
        }






    private class DBholder {
        public TextView dbTitle;
        public TextView dbTitle2;
        public TextView dbTitle3;
        DBholder(View view) {
            dbTitle = view.findViewById(R.id.dbText1);
            dbTitle2=view.findViewById(R.id.dbText2);
            dbTitle3=view.findViewById(R.id.dbText3);
        }
    }
}

