package com.example.proiectdam19;

import android.database.Cursor;
import android.os.AsyncTask;

import java.util.List;

public class Service implements DAOitems {

    private final DAOitems dao;

    public Service(DAOitems item){this.dao=item;}

    @Override
    public Cursor getItems() {
        return dao.getItems();
    }




    @Override
    public Long addItem(Items item) {
        return dao.addItem(item);
    }

    @Override
    public void deleteItem(Items item) {
        dao.deleteItem(item);
    }




}

