package com.example.proiectdam19;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.service.quicksettings.Tile;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Header;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;


import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.graphics.pdf.PdfDocument;

import com.itextpdf.text.PageSize;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    static RoomDataBase rb;
    //    RoomDataBase rb1=RoomDataBase.getINSTANCE(getApplicationContext());
    private Button dBtn;
    private Calendar c;
    private DatePickerDialog dpd;
    private TextView dTV;
    private EditText edName;
    private SeekBar skBar;
    private EditText edPrice;
    private Button savePdf;
    private Button btnDD;


    public SeekBar getSkBar() {
        return skBar;
    }

    public EditText getEdPrice() {
        return edPrice;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dBtn = (Button) findViewById(R.id.btnDate);
        dTV = findViewById(R.id.textViewDate);
        edName = findViewById(R.id.etName);
        skBar = findViewById(R.id.seekBarQ);
        edPrice = findViewById(R.id.editTextPrice);


        dBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = Calendar.getInstance();
                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                dpd = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        dTV.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                    }
                }, year, month, day);
                dpd.show();
            }
        });

    }



    public void btnEnterElem(View view) {
        Items it = new Items();

        String objName = edName.getText().toString();
        it.setName(objName);

        int seek = skBar.getProgress();
        it.setQuantity(seek);

        int price = Integer.parseInt(edPrice.getText().toString());
        it.setTotalPrice(price);

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("itemKey", it);
        startActivity(intent);


    }

}




