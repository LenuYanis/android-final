import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Network extends AsyncTask<URL, Void, InputStream> {
    InputStream ist=null;

    @Override
    protected void onPostExecute(InputStream inputStream) {
        super.onPostExecute(inputStream);
    }

    @Override
    protected InputStream doInBackground(URL... urls) {
        HttpURLConnection conn = null;

        try {
            conn = (HttpURLConnection) urls[0].openConnection();
            conn.setRequestMethod("GET");
            ist = conn.getInputStream();

             } catch (Exception ex) {
            Log.e("doInBackground", ex.getMessage());
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        return ist;
    }
}
